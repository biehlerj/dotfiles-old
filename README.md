# dotfiles

Repository containing all my dotfiles and configurations for Linux.

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Files/Directories](#filesdirectories)
* [Apps I Use](#apps-i-use)
    * [Apps for Development](#apps-for-development)
        * [Text editor/IDE](#text-editoride)
        * [Backend Development:](#backend-development)
        * [Frontend Development:](#frontend-development)
    * [Communication Apps](#communication-apps)
    * [Gaming/Gaming peripherals](#gaminggaming-peripherals)
* [TODOs](#todos)
    * [Finished TODOs](#finished-todos)
* [Improvements](#improvements)
* [Credits](#credits)
* [Usage](#usage)
* [Author](#author)

<!-- vim-markdown-toc -->

## Files/Directories

| Files/Directories | Description                                                                                                                                                                                                                                                     |
|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| browser_settings/ | Configurations for plugins for browsers and bookmarks (TODO)                                                                                                                                                                                                    |
| vimwiki/          | Personal wiki entries from vimwiki to use on all devices                                                                                                                                                                                                        |
| .config/          | Personal config settings for apps I use which are stored in `$XDG_CONFIG_HOME`                                                                                                                                                                                  |
| .local/           | Personal libs, share data, and scripts that I use/are used by apps I use is stored in `$HOME/.local`                                                                                                                                                            |
| basic.vimrc       | Minimal vimrc for development on remote machines with strict policies. Uses built-in vim features to replicate features from my [`.config/nvim/init.vim`](./.config/nvim/init.vim) that I use all the time for small edits, better movement, and reading files. |
| .gitconfig        | My Git configuration settings                                                                                                                                                                                                                                   |

## Apps I Use

- Terminal emulator: I am currently using Alacritty, I have only used it on Linux, but I plan to test it on Mac and Windows.

### Apps for Development

#### Text editor/IDE

I have switched from using PyCharm for Python and VSCode for everything else to using NeoVim for everything. To see what plugins I use for NeoVim and coc.nvim see my vim settings in `.config/nvim/init.vim` and look at the section titled `g:coc_global_extensions`.

I am in the process of documenting what all the stuff in my vimrc does as well as better documenting why I use the coc.nvim extenisons that I do.

#### Backend Development:

- Postman: I use Postman to test my APIs to make sure that requests are accepting the correct types and sending errors and responses correctly. The features are very great, you can use JavaScript in Postman to set variables based on responses from specific requests and reuse them across a set context within Postman. (If you know of a terminal based replacement for Postman let me know as I'm trying to get away from using Electron apps)
- PgAdmin4: A GUI that runs in your browser for managing PostgreSQL databases. Helps for quickly looking at a table and seeing their relationships.

#### Frontend Development:

- Firefox DevTools: I know that almost all frontend devs use Chrome DevTools because they seem to be an industry standard, but I find Firefox's DevTools to be significantly better than Chrome's. The only thing that Chrome has going for it over Firefox is that you can customize the theme of the DevTools.

### Communication Apps

- Discord
- Slack
- Signal
- Keybase

### Gaming/Gaming peripherals

- Steam: Steam is one of the biggest video game distribution services in the world and are one of the biggest proponents of making all video games run natively on Linux. Their product, ProtonDB, is allowing developers to create games and ship them to Steam and allow them to run on Linux without making game developers spend extra time on the game. A significant portion of my games are on Steam.
- Lutris: For all my games I can't find on Steam, I use Lutris which manages games that aren't officially supported on Linux by creating Wine configurations to run a vareity of games.
- Polychromatic: Unofficial Razer device manager for Linux. Allows me to change the RGB scheme of my Razer Ornata Chroma.

## TODOs

- Personalize and modularize configs as best as possible to fit my personalized workflow as I figure out what tiling window manager I want to use. (leaning towards Xmonad)
- Make `kubeconcat` able to take options that allows the user to overwrite the target if it already exists, appends to the target if it already exists, or prompts the user for a different target if the file exists, add a `-h`/`--help` option with text about how to use the script and available options, etc.
- Make `covid19-cli.py` an executable written in Rust that takes a country or state and gives information about COVID-19 that I find interesting to know (ie. mortality rate, survival rate, daily delta, total cases, etc.)
- Modularize `.config/nvim/init.vim` to only load plugins and key maps by filetype to reduce load time, memory usage, and avoid input lag on systems with lower RAM.

### Finished TODOs

- ~~Separate Mac and Windows settings into their own repositories and make this one Linux only.~~ (Done in [this commit](https://gitlab.com/biehlerj/dotfiles/-/commit/a6718b66a0b05417947e5066962e00bd82607440))
- ~~Clean up home directory things by moving more items into `.local` & `.config` directories.~~ (Finished over a series of commits ending with [this commit](https://gitlab.com/biehlerj/dotfiles/-/commit/e1e90803ba5bc289745e749f648bd013bc88412a))
- ~~Update dotfiles to be more modular and inline with how they are supposed to be used, for instance storing PATH variables in `.profile` or `.zprofile`.~~ (Done in [this commit](https://gitlab.com/biehlerj/dotfiles/-/commit/1f72b594e2097a8f112d7763c30fef4dc3eca781))
- ~~Get rid of ohmyzsh.~~ (Done in [this commit](https://gitlab.com/biehlerj/dotfiles/-/blob/1e6abded4b3be540b793c79b1667bc9d150351bf/.config/zsh/.zshrc))
- ~~Add coc plugins as a list in `.vimrc` or `init.vim` instead of keeping a list in a markdown file.~~ (Done in [this commit](https://gitlab.com/biehlerj/dotfiles/-/commit/01d5208bad7b1a45c2b12282869ed40ced242147))

## Improvements

- Add screen shots of desktop environments and select apps to show what my dotfiles do to my systems. Add gifs for things like zsh and nvim.
- Make as many configuration files universal as possible by using shell variables that can be configured by system.
- Get rid of GUI apps in favor of CLI apps (ie. TaskWarrior over Boostnote)
- Remove the maximum number of Electron apps as possible from apps list in order to save RAM on systems with less RAM and make things available across as many platforms as possible (ie Android, Raspberry Pi, etc.)

## Credits

A lot of configs and scripts in this repository were taken from or inspired by people like Distrotube, Brodie Robertson, and Luke Smith. When possible if I just took a config or script from someone I gave them credit in a comment with a link to the repository where I got it from. For scripts and configs that were inspired by someone where possible I left a comment to the file, video, or article where I got the inspiration from. Some of the older ones do not have a reference to the person, video, file, or article I found my configs in or were inspired by as there were just so many that I've added over the years where I forgot where I got them from, if this applies to you please leave an issue with what file or line where you found the code I used and a link you'd like me to leave in the comment and I'll be happy to give you credit.

## Usage

Feel free to use any of the settings or apps I have in this repository for yourself. If you do use something you found in this repo feel free to drop me a mention on [my Twitter](https://twitter.com/Biehlerj) or if you use [Brave](https://brave.com/features) with [Brave Rewards enabled](https://brave.com/brave-rewards/) feel free to leave me a tip. If you have any issues with something you are using from this repo leave an issue on the [issues page](https://gitlab.com/biehlerj/dotfiles/issues) with the error you are experiencing and with what and I'll do my best to help you out.

## Author

[Jacob Biehler](https://www.linkedin.com/in/jacob-biehler-475573139/)

[Twitter](https://twitter.com/Biehlerj)

[GitLab](https://gitlab.com/biehlerj)
