if (has("nvim"))
        let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif
if (has("termguicolors"))
        set termguicolors
endif

syntax enable

" --------------------------------------------------------------------------------
"  configure editor with tabs and nice stuff
"  --------------------------------------------------------------------------------
" Show absolute line number of current line and
" relative line numbers of all other lines
set number relativenumber
set nocompatible
filetype plugin indent on
set expandtab
set ruler
set visualbell                  " Don't beep please
set noerrorbells                " Don't beep please
set autoread                    " Detect changes to a file outside of vim and read in changes

" Cursor settings
set guicursor=a:ver25
set encoding=utf8
hi Cursor gui=NONE guifg=bg guibg=fg
hi Normal guibg=NONE ctermbg=NONE

" Toggle invisible characters
set list
set listchars=tab:→\ ,eol:¬,trail:⋅,extends:❯,precedes:❮
set showbreak=↪

" Make scripts executable on save
au BufWritePost * if getline(1) =~ "^#!" | silent !chmod +x <afile> | endif

" Make backspaces more powerful
set backspace=indent,eol,start

" Splits open at the bottom and right
set splitbelow splitright

" --------------------------------------------------------------------------------
"  Custom keybinds, keymaps, and autocmds
"  --------------------------------------------------------------------------------
" Easier split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Toggle highlight search
set hlsearch!
nnoremap <leader>hl :set hlsearch!<CR>

" Accidentally hitting unwanted keys in normal mode:
nnoremap <F1> <nop>
" lets do the same for insert mode!!!
inoremap <F1> <nop>

" Switch ; and : to make entering commands easier
nnoremap  ;  :
nnoremap  :  ;

" Remove newbie crutches in Command Mode
cnoremap <Down> <Nop>
cnoremap <Left> <Nop>
cnoremap <Right> <Nop>
cnoremap <Up> <Nop>

" Remove newbie crutches in Insert Mode
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>
inoremap <Up> <Nop>

" Remove newbie crutches in Normal Mode
nnoremap <Down> <Nop>
nnoremap <Left> <Nop>
nnoremap <Right> <Nop>
nnoremap <Up> <Nop>

" Automatically deletes all trailing whitespace and newlines at end of file on save.
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritepre * %s/\n\+\%$//e

" Vertically center document when entering insert mode
autocmd InsertEnter * norm zz

" --------------------------------------------------------------------------------
"  Netrw settings
"  --------------------------------------------------------------------------------
" Hit enter in the file browser to open the selected
" file with :vsplit to the right of the browser
let g:netrw_browse_split = 4

" Absolute width of netrw window
let g:netrw_winsize = -28

" Tree-view
let g:netrw_liststyle = 3

" Sort is affecting only: directories on the top, files below
let g:netrw_sort_sequence = '[\/]$,*'

" Change directory to the current buffer when opening files.
set autochdir

" Toggle Vexplore with Ctrl-N
function! ToggleVExplorer()
    if exists("t:expl_buf_num")
        let expl_win_num = bufwinnr(t:expl_buf_num)
        let cur_win_num = winnr()

        if expl_win_num != -1
            while expl_win_num != cur_win_num
                exec "wincmd w"
                let cur_win_num = winnr()
            endwhile

            close
        endif

        unlet t:expl_buf_num
    else
         Vexplore
         let t:expl_buf_num = bufnr("%")
    endif
endfunction

noremap <silent> <C-n> :call ToggleVExplorer()<CR>
