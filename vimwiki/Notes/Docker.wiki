= Docker Notes =

== Docker Run options ==

* `--rm`: makes a container transitive (auto-delete on close)
* `-it`: makes the container interactive and tty. Allows you to connect to the container via terminal in order to interact with the container
* `-e`: set environment variables in the container. (Make sure to set these before the `-it` option)
* `v`: bind mount a volume. (Make sure to set these before the `-it` option)
* To open up a Docker container to make requests to run `docker run -e [env variables here] -v [data to mount here] -it --rm -p 127.0.0.1:5000:5000/tcp [name of container]`
    * That maps host port 5000 to container port 5000, restricting the interface to 127.0.0.1 (no one outside your machine can use it)
    * Most quick guides say to use `-p 5000:5000`, but that opens your host firewall for global access. It also modifies iptables in a way that neither UFS (debian-based systems) or Firewalld (RHEL/Fedora-based systems) will not indicate it's open.
* Technically launching Docker is an admin action, so many guides suggest adding your user to the `docker` group so you don't need to use `sudo` to run docker commands. This can potentially be a security risk because it gives the user passwordless sudo since they can launch any docker command, mount the host's protected filesystems, and do whatever they want.
    * Look into the security implications of adding your user to the `docker` group

== Docker Build options ==

* `-t`: provides a name for the image. Image name syntax is `<private-repo/><group-and-or-name>:<tag-aka-version>`. If `<private-repo/>` is skipped, any attempt to push/pull the image will presumes the public dockerhub
    * Example: `dockerhub.moesol.com/foo/bar:1.2.3...` where `dockerhub.moesol.com` is our private repo, `foo/bar` is our image name with group prefix, and 1.2.3 is our tag
