= Oneshot notes =

== Map of Faerun ==

https://i.pinimg.com/originals/1b/21/4e/1b214e51aa7d57f029b82633400d7629.jpg

== Characters ==

* Delric: Elf who is giving us our quest.
* Lort Mortis Grimel: The leader of the Faerun faction of the Night Masks.

== Quest ==

* Kill the Night Masks: 300g and a health potion each.
    * Need to leave one of them alive.
    * Dress up as Zhentarim to make it look like they did it.
    * Raiding a cart, the primary objective to get a crate from Chult (very large).
    * Crate must be undamaged.
    * Supposedly the contents of the crate should not exist.
    * Cart comes into city at dusk.
        * Contents of cart:
            * Unknown by Delric
            * A Raptor egg

== Inventory ==

* Zhentarim clothes
* Small cloak double or half cloak. (Ask about stats next session)
* Interesting looking ring. (Ask about stats next session)

== Night Masks ==

* Marked as a servant to a demon lord when they have shadowy marks.
* The Night Masks are lead by a council of vampires. The faction in this region are lead by Lord Mortis Grimel.
